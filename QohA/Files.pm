package QohA::Files;

use Moo;
use Modern::Perl;
use List::MoreUtils qw(uniq);

use QohA::File;
use QohA::File::XML;
use QohA::File::Perl;
use QohA::File::Template;
use QohA::File::JS;
use QohA::File::Vue;
use QohA::File::YAML;
use QohA::File::Specific::Sysprefs;
use QohA::File::Specific::Kohastructure;
use QohA::File::Specific::SCSS;
use QohA::File::Invalid;

has 'files' => (
    is => 'rw',
);

sub BUILD {
    my ( $self, $param ) = @_;
    my @files = @{$param->{files}};
    $self->files([]);
    for my $f ( @files ) {
        my $filepath = $f->{filepath};
        my $params = {
            path     => $filepath,
            new_path => $f->{new_filepath},
        };
        my $file;
        if ( $filepath =~ qr/\.xml$|\.xsl$|\.xslt$/i ) {
            $file = QohA::File::XML->new(%$params);
        } elsif ( $filepath =~ qr/\.tt$|\.inc$/i ) {
            $file = QohA::File::Template->new(%$params);
        } elsif ( $filepath =~ qr/\.pl$|\.pm$|\.t$|svc|unapi$/i ) {
            $file = QohA::File::Perl->new(%$params);
        } elsif ( $filepath =~ qr/\.js$|\.ts$/i ) {
            $file = QohA::File::JS->new(%$params);
        } elsif ( $filepath =~ qr/\.vue$/i ) {
            $file = QohA::File::Vue->new(%$params);
        } elsif ( $filepath =~ qr/\.yml$|\.yaml$/i ) {
            $file = QohA::File::YAML->new(%$params);
        } elsif ( $filepath =~ qr/sysprefs\.sql$/ ) {
            $file = QohA::File::Specific::Sysprefs->new(%$params);
        } elsif ( $filepath =~ qr/kohastructure\.sql$/ ) {
            $file = QohA::File::Specific::Kohastructure->new(%$params);
        } elsif ( $filepath =~ qr/\.scss$/ ) {
            $file = QohA::File::Specific::SCSS->new(%$params);
        } elsif ( $filepath =~ qr/\.sw(p|o)$/ ) {
            $file = QohA::File::Invalid->new(%$params);
        } elsif ( $f->{statuses} =~ m|A| and $f->{statuses} =~ m|D| ) {
            $file = QohA::File->new(%$params);
        }
        next unless $file;
        $file->git_statuses( $f->{statuses} );
        $file->new_file( 1 ) if $f->{statuses} =~ m|A|;
        push @{ $self->files}, $file;
    }
}

1;

=head1 AUTHOR
Mason James <mtj at kohaaloha.com>
Jonathan Druart <jonathan.druart@biblibre.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2012 by KohaAloha

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007
=cut
