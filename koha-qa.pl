#!/usr/bin/perl -w

our ($v, $d, $c, $nocolor, $help, $no_progress, $failures_only, $run_tests);

BEGIN {
    use Getopt::Long;
    use Pod::Usage;

    our $r = GetOptions(
        'v:s'         => \$v,
        'c:s'         => \$c,
        'd'           => \$d,
        'no-progress' => \$no_progress,
        'nocolor'     => \$nocolor,
        '--failures'  => \$failures_only,
        '--run-tests' => \$run_tests,
        'h|help'      => \$help,
    );
    pod2usage(1) if $help;

    $v = 0 if not defined $v or $v eq '';
    $nocolor = 0 if not defined $nocolor;
}

use Modern::Perl;
use Getopt::Long;
use Module::Load::Conditional qw[can_load];
use Email::Address;
use IPC::Cmd qw( run );

use QohA::Git;
use QohA::Files;

BEGIN {
    eval "require Test::Perl::Critic::Progressive";
    die
"Test::Perl::Critic::Progressive is not installed \nrun:\ncpan install Test::Perl::Critic::Progressive\nto install it\n"
      if $@;
}

our @tests_to_skip = ();
# Assuming codespell is in /usr/bin
unless ( -f '/usr/bin/codespell' ) {
    warn "You should install codespell\n";
    push @tests_to_skip, 'spelling';
}
unless ( can_load( modules => { 't::lib::QA::TemplateFilters' => undef } ) ) {
    warn "You are using a version of Koha that does not contain bug 13618 and/or bug 21393. Missing filters check will be skipped.\n";
    push @tests_to_skip, 'filters';
}

if ( -f '.git/hooks/git-store-meta.pl' ) {
    run( command => '.git/hooks/git-store-meta.pl --store -f user,group,mode,mtime,atime', verbose => 0 );
}

my $git = QohA::Git->new();
if ( @{$git->diff_log} ) {
    say "Cannot launch QA tests: You have unstaged changes.\nPlease commit or stash them.";
    exit 1;
}

our $branch = $git->branchname;
unless ( $branch ) {
    say "Cannot launch QA tests: You are not currently on a branch.";
    exit 1;
}

my ( $new_fails, $already_fails, $error_code, $full ) = 0;

eval {

    $c //= QohA::Git::num_of_commits_last_bz_number();
    my $num_of_commits = $c;

    print QohA::Git::log_as_string($num_of_commits);

    my $log_files = $git->log($num_of_commits);
    my $modified_files = QohA::Files->new( { files => $log_files } );

    $git->delete_branch( 'qa-prev-commit' );
    $git->create_and_change_branch( 'qa-prev-commit' );
    $git->reset_hard_prev( $num_of_commits );

    my @files = @{ $modified_files->files };
    my $i = 1;
    say "Processing files before patches";
    for my $f ( @files ) {
        unless ( $no_progress ) {
            print_progress_bar( $i, scalar(@files) );
            $i++;
        }
        $f->run_checks();
    }

    $git->change_branch($branch);
    $git->delete_branch( 'qa-current-commit' );
    $git->create_and_change_branch( 'qa-current-commit' );

    {
        # If a module has been modified by the patchset we reload it
        local $SIG{__WARN__} = sub {
            my $warning = shift;
            warn $warning unless $warning =~ /Subroutine .* redefined at/;
        };
        for my $file (@files) {
            next if $file->{path} !~ m|\.pm$|;
            my $module = $file->{path};
            delete $INC{$module};
            $module =~ s|/|::|g;
            $module =~ s|\.pm$||;
            eval "require $module";
            $module->import;
        }
    }

    $i = 1;
    say "\nProcessing files after patches";
    for my $f ( @files ) {
        unless ( $no_progress ) {
            print_progress_bar( $i, scalar(@files) );
            $i++;
        }
        $f->run_checks($num_of_commits);
    }
    say "\n" unless $no_progress;

    for my $f ( sort { $a->path cmp $b->path } @files ) {
        say $f->report->to_string(
            {
                verbosity => $v,
                color     => not( $nocolor ),
                skip      => \@tests_to_skip,
                failures_only => $failures_only,
            }
        );
    }

    print "\nProcessing additional checks";
    my @log_formats = `git log --oneline -$num_of_commits`;
    my @errors;
    for my $log_format ( @log_formats ) {
        my ( $sha, @commit_title ) = split ' ', $log_format;
        my $commit_title = join ' ', @commit_title;
        if ( $commit_title !~ m|^Bug\s\d{4,5}: | ) {
            push @errors, "Commit title does not start with 'Bug XXXXX: ' - $sha";
        }
        if ( $commit_title =~ m|follow-?up|i ) {
            if ( $commit_title !~ m|follow-up\)| ) {
                push @errors, "Commit title does not contain 'follow-up' correctly spelt - $sha";
            }
            if ( $commit_title =~ m|qa.?follow.?up|i and not $commit_title =~ m|\(QA follow-up\)| ) {
                push @errors, "Commit title does not contain '(QA follow-up)' correctly spelt - $sha";
            }
        }
    }

    my @authors = `git log --pretty="%an" -$num_of_commits`;
    for my $author ( @authors ) {
        chomp $author;
        push @errors, "Author '$author' seems invalid"
            if   $author =~ m|koha|i
              or $author =~ m|bywater|i
              or $author =~ m|John Doe|i
              or $author eq '='
          ;
    }

    my @emails = `git log --pretty="%ae" -$num_of_commits`;
    for my $email ( @emails ) {
        chomp $email;
        push @errors, "Email's author '$email' seems invalid"
            if   $email =~ m|koha-?dev|
              or $email =~ m|localhost|
              or $email =~ m|example|
              or $email =~ m|root|
          ;
        push @errors, "Email's author '$email' seems invalid"
            unless is_email_valid($email);

    }

    my @signed_off_by_lines = `git log --pretty="%b" -$num_of_commits | grep Signed-off-by`;
    for my $sobl ( @signed_off_by_lines ) {
        chomp $sobl;
        next unless $sobl;
        if ( $sobl =~ m|^Signed-off-by: .*<(.*)>$| ) {
            my $email = $1;
            push @errors, "Signed-off-by line '$sobl' seems invalid"
                unless is_email_valid($email);
        } else {
            push @errors, "Signed-off-by line '$sobl' seems invalid";
        }
    }

    if ( @errors ) {
        say "\n";
        say "\t* $_" for @errors;
    } else {
        say " OK!";
    }

    if ( $run_tests ) {
        print "\nRunning tests";
        my @test_files = grep { $_->filename =~ qr/\.t$/ && $_->git_statuses !~ m{D} } @files; # Test files that have not been deleted
        if ( @test_files ) {
            say sprintf " (%d)", scalar @test_files;
            for my $f ( @test_files ) {
                print "\t* Proving " . $f->new_abspath;
                my $cmd = 'KOHA_TESTING=1 prove ' . $f->new_abspath;
                my ( $success, $error_code, $full_buf, $stdout_buf, $stderr_buf ) = run( command => $cmd, verbose => 0 );
                say " OK!" if $success;
                say " KO!\n@$full_buf" unless $success;
            }
        } else {
            say "\n\t* No test files found.";
        }
    }
};

if ($@) {
    say "\n\nAn error occurred : $@";
}

$git->change_branch($branch);

exit(0);

sub print_progress_bar {
    my ( $progress, $total ) = @_;
    my $num_width = length $total;
    print sprintf "|%-25s| %${num_width}s / %s (%.2f%%)\r",
        '=' x (24*$progress/$total). '>',
        $progress, $total, 100*$progress/+$total;
    flush STDOUT;
}

sub is_email_valid {
    my ( $email ) = @_;
    my @addrs = Email::Address->parse($email);
    return @addrs ? 1 : 0;
}


__END__

=head1 NAME

koha-qa.pl

=head1 SYNOPSIS

koha-qa.pl -c NUMBER_OF_COMMITS [-v VERBOSITY_VALUE] [-d] [--failures] [--nocolor] [-h]


=head1 DESCRIPTION

koha-qa.pl runs various QA tests on the last $x commits, in a Koha git repo.

refer to the ./README file for installation info

=head1 OPTIONS

=over 8

=item B<-h|--help>

prints this help message

=item B<-v>

change the verbosity of the output
    0 = default, only display the list of files
    1 = display for each file the list of tests
    2 = display for each test the list of failures

=item B<-c>

Number of commit to test from HEAD

=item B<-d>

Debug mode

=item B<--failures>

Only display failures.

=item B<--nocolor>

do not display the status with color

=back

=head1 AUTHOR

Mason James <mtj at kohaaloha.com>
Jonathan Druart <jonathan.druart at biblibre.com>

=head1 COPYRIGHT

This software is Copyright (c) 2012 by KohaAloha and BibLibre

=head1 LICENSE

This file is part of Koha.

Koha is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or (at your option) any later version.

You should have received a copy of the GNU General Public License along
with Koha; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

=head1 DISCLAIMER OF WARRANTY

Koha is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

=cut
