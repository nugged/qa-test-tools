package QohA::Git;

use Modern::Perl;

use Moo;

has 'branchname' => (
    is => 'rw',
    default => sub{ get_current_branch() },
);

# this sub returns all modified files, for testing on
sub log {
    my ($self, $cnt) = @_;

    my @r = qx{git log --oneline --name-status -$cnt};
    my $files = {};

    foreach my $rr ( reverse @r ) {
        chomp $rr;
        my @cols = split '\t', $rr ;

        # ignore lines that are commit shas, not filename
        next if not defined $cols[1];

        my $statuses = $cols[0];
        my $new_filepath = $statuses =~ m{D} ? undef : $statuses =~ m{R} ? $cols[2] : $cols[1];

        # We are keeping the statuses and new filepath of the last patch modifying this file!
        $files->{$cols[1]} = {
            filepath => $cols[1],
            statuses => $statuses,
            new_filepath => $new_filepath,
        };
    }

    return [ values %$files ];
}

# Return the number of commits that matches the first (HEAD) commit's bug number
# Max is 100
sub num_of_commits_last_bz_number {
    my ($self) = @_;

    my @r = qx{git log --oneline -100};

    my ( $bug_number, $i );
    foreach my $rr ( @r ) {
        # Should be "sha Bug XXXXX:"
        chomp $rr;
        my @cols = split '\s', $rr ;
        ( my $cur_bn = $cols[2] ) =~ s|\D||g;
        $bug_number //= $cur_bn;
        last if $bug_number ne $cur_bn;
        $i++;
    }
    return $i;
}

sub diff_log {
    my ($self, $cnt, $file) = @_; # $file is optionnal
    my $cmd = $cnt
        ? qq{git diff HEAD~$cnt..}
        : q{git diff HEAD};
    $cmd .= qq{ $file} if $file;
    my @r = qx/$cmd/;
    chomp for @r;
    return \@r;
}

sub log_as_string {
    my ($cnt) = @_;

    my @logs = qx{git log --oneline --numstat -$cnt};

    my $r;
    my ( $cc, $cdesc ) = get_prev_commit($cnt);

    chomp $cdesc;
    $cdesc = substr $cdesc, 0, 37;
    $r .= "testing $cnt commit(s) (applied to $cc '$cdesc')\n";

=c
    # there is no need for this display code,
    # the filenames are displayed elsewhere...

    my $i = 0;
    foreach my $l (@logs) {
        chomp $l;

        my @a = split '\t', $l;
        my ( $sha, $diff, $action, $filename, $desc );

        if ( $a[0] =~ /^\w{7} / and not defined $a[2] ) {
            $desc = $a[0];
            $desc = substr $desc, 0, 77;
            $r .= "\n $desc\n";
        }
        else {
            $diff     = $a[0];
            $filename = $a[2];
#            $r .= " - $filename\n" if $filename;
        }
        $i++;
    }
=cut

    return "$r\n";
}

sub create_and_change_branch {
    my ($self, $branchname) = @_;
    qx|git checkout -b $branchname 2> /dev/null|;
}

sub change_branch {
    my ($self, $branchname) = @_;
    qx|git checkout $branchname 2> /dev/null|;
}

sub delete_branch {
    my ($self, $branchname) = @_;
    qx|git branch -D $branchname 2> /dev/null|;
}

sub reset_hard_prev {
    my ($self, $cnt) = @_;
    qx|git reset --hard HEAD~$cnt 2> /dev/null|;
}

#FIXME There is no a simple way to get the current branch
sub get_current_branch {
    my $br = qx{git symbolic-ref --short HEAD 2> /dev/null};
    chomp $br;
    return $br;
}

sub get_prev_commit {
    my ($cnt) = @_;
    my $c =
      qx{git log --abbrev-commit --format=oneline -n 1 HEAD~$cnt};

    my $cc = substr $c , 0, 7;
    my $cdesc = substr $c , 8;

    return $cc, $cdesc;

}

1;

=head1 AUTHOR
Mason James <mtj at kohaaloha.com>
Jonathan Druart <jonathan.druart@biblibre.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2012 by KohaAloha

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007
=cut
